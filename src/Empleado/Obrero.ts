import { Empleado } from "./Empleado";
import { Tiempo } from "../Tiempo";

export class Obrero extends Empleado {
    establecerHoraSalida() {
        let t = new Tiempo(this.horaEntrada.hora, this.horaEntrada.min, this.horaEntrada.seg);
        let ti = t.sumarHoras(8, t);
        ti = ti.sumarMinutos(30, ti);
        this._horaSalida = new Tiempo(ti.hora, ti.min, ti.seg);
    }
}
