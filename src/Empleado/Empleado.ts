import { Tiempo } from '../Tiempo';
import { Huella } from '../Huella';
export class Empleado {
    private _nombre: string;
    public get nombre(): string {
        return this._nombre;
    }
    public set nombre(value: string) {
        this._nombre = value;
    }
    private _identificacion: string;
    public get identificacion(): string {
        return this._identificacion;
    }
    public set identificacion(value: string) {
        this._identificacion = value;
    }
    private _dactilar: Huella;
    public get dactilar(): Huella {
        return this._dactilar;
    }
    public set dactilar(value: Huella) {
        this._dactilar = value;
    }
    private _horaIngreso: Tiempo;
    public get horaIngreso(): Tiempo {
        return this._horaIngreso;
    }
    public set horaIngreso(value: Tiempo) {
        this._horaIngreso = value;
    }
    private _horaEntrada: Tiempo;
    public get horaEntrada(): Tiempo {
        return this._horaEntrada;
    }
    public set horaEntrada(value: Tiempo) {
        this._horaEntrada = value;
        this.establecerHoraSalida();
    }
    protected _horaSalida: Tiempo;
    public get horaSalida(): Tiempo {
        return this._horaSalida;
    }
    protected jornadaH: number = 12;
    protected jornadaM: number = 0;

    establecerHoraSalida() {
        let t = new Tiempo(this.horaEntrada.hora, this.horaEntrada.min, this.horaEntrada.seg);
        let ti = t.sumarHoras(this.jornadaH, t);
        this._horaSalida = new Tiempo(ti.hora, ti.min, ti.seg);
    }

}
