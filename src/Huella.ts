export class Huella {
    private _dedo: string;
    private _imagen: string;

    public get dedo(): string {
        return this._dedo;
    }

    public get imagen(): string {
        return this._imagen;
    }
}