import * as $ from 'jquery'
import { Obrero } from './Empleado/Obrero';
import { Tiempo } from './Tiempo';
import { Huella } from './Huella';

$("#agregar").on("click", (e) => {
	$("#usuario").append(`

		<input type="text" placeholder="nombre" id="nombre">
		<input type="number" placeholder="Hora entrada" id="hora">
		<input type="number" placeholder="Minuto entrada" id="minuto">
		<input type="number" placeholder="Segundo entrada" id="segundo">
		<button id="guardar">Guardar</button>

	`);
});

$('#usuario').on('click', 'button', (e) => {
	let nombre = $("#nombre").val();
	let identificacion = $("#identificacion").val();
	let hora = $("#hora").val();
	let minuto = $("#minuto").val();
	let segundo = $("#segundo").val();
	var objHE: Tiempo = new Tiempo(Number(hora), Number(minuto), Number(segundo));
	var objEO: Obrero = new Obrero();
	objEO.horaIngreso = objHE;
	objEO.horaEntrada = objHE;
	objEO.nombre = String(nombre);
	$("#usuario").append(`

		Debe salir a las ${objEO.horaSalida.imprimirEstandar()}

	`);
	console.log(objEO.horaSalida.imprimirEstandar());
});

