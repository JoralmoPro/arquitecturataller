import { String } from 'typescript-string-operations';
export class Tiempo {
    private _hora: number;
    private _min: number;
    private _seg: number;

    public get hora(): number {
        return this._hora;
    }

    public get min(): number {
        return this._min;
    }


    public get seg(): number {
        return this._seg;
    }

    constructor(hora?: number, min?: number, seg?: number) {
        this._hora = (hora >= 0 && hora < 24) ? hora : 0 || 0;
        this._min = (min >= 0 && min < 59) ? min : 0 || 0;
        this._seg = (seg >= 0 && seg < 59) ? seg : 0 || 0;
    }

    public imprimirEstandar(): string { //Esto ya estaba
        let h: number = this.hora;
        let horario: string = "A.M";
        if (h > 12) {
            h = h - 12;
            horario = "P.M";
        }
        return String.Format(`{0:00}:{1:00}:{2:00} ${horario}`, h, this.min, this.seg);
    }

    public imprimirMilitar(): string { //Esto tambien
        return String.Format(`{0:00}:{1:00}:{2:00}`, this.hora, this.min, this.seg);
    }

    public sumarHoras(h: number, t: Tiempo): Tiempo {
        let hTemp = (t.hora + h > 23) ? t.hora + h - 24 : t.hora + h;
        let ti = new Tiempo(hTemp, t.min, t.seg);
        return ti;
    }

    public sumarMinutos(m: number, t: Tiempo): Tiempo {
        let mult = (parseInt((m / 60).toString())) != 0 ? (parseInt((m / 60).toString())) : 1;
        let mTemp = (t.min + m > 59) ? t.min + m - (60 * mult) : t.min + m;
        let ti = new Tiempo(t.sumarHoras(parseInt(((m + t.min) / 60).toString()), t).hora, mTemp, t.seg);
        return ti;
    }

}
